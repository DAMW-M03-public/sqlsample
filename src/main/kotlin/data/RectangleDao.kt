package data

import models.Rectangle
import java.sql.Connection
import java.sql.ResultSet


class RectangleDao(val database: RectangleDatabase) {
    val connection: Connection get() = database.connection!!

    fun createTableIfNotExists() {
        val createQuery = "CREATE TABLE IF NOT EXISTS rectangles(id INTEGER PRIMARY KEY AUTOINCREMENT, width INTEGER, height INTEGER)"
        val createStatement = connection.prepareStatement(createQuery)
        createStatement.execute()
    }

    fun list(): List<Rectangle> {
        val query = "SELECT * FROM rectangles"
        val listStatement = connection.createStatement()
        val resultat: ResultSet = listStatement.executeQuery(query)
        return toRectangleList(resultat)
    }


    fun insert(rectangle: Rectangle) {
        val query = "INSERT INTO rectangles(width, height) VALUES(?, ?)"
        val statment = connection.prepareStatement(query)
        statment.setInt(1, rectangle.width)
        statment.setInt(2, rectangle.height)
        statment.execute()
    }

    /**
     * Converts result set to list of rectangles
     */
    private fun toRectangleList(result: ResultSet): List<Rectangle> {
        val list: MutableList<Rectangle> = mutableListOf()
        while (result.next()) {
            list += toRectangle(result)
        }
        return list
    }

    /**
     * Converts result set to a rectangle on the current position of the set
     */
    private fun toRectangle(result: ResultSet): Rectangle {
        val id = result.getInt("id")
        val width = result.getInt("width")
        val height = result.getInt("height")
        return Rectangle(id, width, height)
    }


}