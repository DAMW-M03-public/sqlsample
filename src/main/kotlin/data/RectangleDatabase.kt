package data

import java.sql.Connection
import java.sql.DriverManager

val databaseFile = "sample.db"
class RectangleDatabase {
    var connection: Connection? = null

    fun connect(): Connection {
        connection = DriverManager.getConnection("jdbc:sqlite:$databaseFile")
        return connection!!
    }
}