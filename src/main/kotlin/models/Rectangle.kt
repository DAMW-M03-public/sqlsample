package models

data class Rectangle(val id: Int, val width: Int, val height: Int){
    constructor(width: Int, height: Int) : this(0, width, height)
}