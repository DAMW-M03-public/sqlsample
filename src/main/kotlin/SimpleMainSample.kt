
import models.Rectangle
import java.sql.DriverManager
import java.sql.PreparedStatement
import java.sql.ResultSet
import java.sql.Statement


/**
 * Sample usage of jdbc inside a single function
 */
fun main() {
    val databaseFile = "sample.db"
    DriverManager.getConnection("jdbc:sqlite:$databaseFile").use{
            connection ->
        val createQuery = "CREATE TABLE IF NOT EXISTS rectangles(id INTEGER PRIMARY KEY AUTOINCREMENT, width INTEGER, height INTEGER)"
        val createStatement: PreparedStatement = connection.prepareStatement(createQuery)
        createStatement.execute()

        val insertQuery = "INSERT INTO rectangles(width, height) VALUES(?, ?)"
        val insertStatement: PreparedStatement = connection.prepareStatement(insertQuery)
        insertStatement.setInt(1, 34)
        insertStatement.setInt(2, 77)
        insertStatement.execute()

        val listQuery = "SELECT * FROM rectangles"
        val listStatement: Statement = connection.createStatement()
        val result: ResultSet = listStatement.executeQuery(listQuery)
        val list: MutableList<Rectangle> = ArrayList()
        while (result.next()) {
            val id = result.getInt("id")
            val width = result.getInt("width")
            val height = result.getInt("height")
            list.add(Rectangle(id, width, height))
        }
        println(list)

//        val deleteQuery = "DELETE FROM rectangles WHERE id = ?"
//        val deleteStatement = connection.prepareStatement(deleteQuery)
//        deleteStatement.setInt(1, id)
//        deleteStatement.execute()
//
//        val updateQuery = "UPDATE rectangles SET(width, height) = (?, ?) WHERE id = ?"
//        val updateStatement = connection.prepareStatement(updateQuery)
//        updateStatement.setInt(1, r.getWidth())
//        updateStatement.setInt(2, r.getHeight())
//        updateStatement.setInt(3, r.getId())
//        updateStatement.execute()
    }


}