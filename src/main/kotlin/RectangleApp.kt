import data.RectangleDao
import data.RectangleDatabase
import models.Rectangle
import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    val database = RectangleDatabase()
    RectangleApp(scanner, database).start()
}

class RectangleApp(val scanner: Scanner = Scanner(System.`in`), val database: RectangleDatabase = RectangleDatabase()){
    val rectangleDao = RectangleDao(database)
    fun start(){
        database.connect().use { connection ->
            rectangleDao.createTableIfNotExists()
            executeOperations()
        }
    }
    fun executeOperations() {
        while(true){
            when(scanner.nextInt()){
                0 -> insert()
                1 -> listAll()
                -1 -> return
            }
        }
    }

    private fun listAll() {
        rectangleDao.list().forEach{ println(it) }
    }

    private fun insert() {
        rectangleDao.insert(readRectangle(scanner))
    }
    private fun readRectangle(scanner: Scanner): Rectangle {
        return Rectangle(scanner.nextInt(), scanner.nextInt())
    }
}




